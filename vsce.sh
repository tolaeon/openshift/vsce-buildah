#!/bin/sh

CONTAINER=$(buildah from node:12-alpine)
buildah run $CONTAINER -- npm install -g vsce
buildah config -v /workspace --workingdir /workspace --entrypoint '["vsce"]' $CONTAINER
buildah commit --squash $CONTAINER $CI_REGISTRY_IMAGE
